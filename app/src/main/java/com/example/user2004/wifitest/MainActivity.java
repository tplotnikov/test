package com.example.user2004.wifitest;

import android.content.IntentFilter;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private String LOG_TAG = "Main";


    private static final String wifiSSID = "SML_Mobile";
    private static final String KEY = "02720272sml";

    private Button btnConnect;
    private NetworkMonitor mNetworkMonitor;
    private WifiManager wifiManager;



////////////////////////////////////////////////////


//    private RecyclerView mRecyclerView;
//    private RecyclerView.Adapter mAdapter;
//    private RecyclerView.LayoutManager mLayoutManager;
//
//    mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
//
//    // use this setting to improve performance if you know that changes
//    // in content do not change the layout size of the RecyclerView
//    mRecyclerView.setHasFixedSize(true);
//
//    // use a linear layout manager
//    mLayoutManager = new LinearLayoutManager(this);
//    mRecyclerView.setLayoutManager(mLayoutManager);
//
//    // specify an adapter (see also next example)
//    mAdapter = new MyAdapter(myDataset);
//    mRecyclerView.setAdapter(mAdapter);



        //translateText = (TextView) findViewById(R.id.translateText);
        //inputText = (EditText) findViewById(R.id.inputText);
        //ok = (Button) findViewById(R.id.buttonOk);



    /////////////////////////////////////////////////


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        btnConnect = (Button) findViewById(R.id.btnConnect);

        wifiManager = (WifiManager) getSystemService(WIFI_SERVICE);
        btnConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                connectToWifi();
                mNetworkMonitor = new NetworkMonitor();
                IntentFilter intent = new IntentFilter();
                intent.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
                registerReceiver(mNetworkMonitor, intent);


            }
        });
    }

    private void connectToWifi() {

        WifiConfiguration wifiConfig = new WifiConfiguration();
        wifiConfig.SSID = String.format("\"%s\"", wifiSSID);
        wifiConfig.preSharedKey = String.format("\"%s\"", KEY);
        int netId = wifiManager.addNetwork(wifiConfig);
        if (wifiManager.isWifiEnabled()) {
            wifiManager.disconnect();
        } else {
            wifiManager.setWifiEnabled(true);
        }
        wifiManager.enableNetwork(netId, true);
        wifiManager.reconnect();
        wifiManager.saveConfiguration();
        WifiInfo connectionInfo = wifiManager.getConnectionInfo();
        Log.d(LOG_TAG, connectionInfo.getSSID());
    }


    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }
}



