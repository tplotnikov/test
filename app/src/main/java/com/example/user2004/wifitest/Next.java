package com.example.user2004.wifitest;

/**
 * Created by user2004 on 21.04.16.
 */
public class Next {
    private String textSize;
    private String textColor;
    private String leftPadding;
    private Next next;


    public String getTextSize() {
        return textSize;
    }

    public void setTextSize(String textSize){
        this.textSize= textSize;
    }

    public String getTextColor(){
        return textColor;
    }

    public void setTextColor(String textColor) {
        this.textColor = textColor;
    }

    public String getLeftPadding() {
        return leftPadding;
    }

    public void setLeftPadding(String leftPadding) {
        this.leftPadding = leftPadding;
    }

    public Next getNext() {
        return next;
    }

    public void setNext(Next next) {
        this.next = next;
    }
}
