package com.example.user2004.wifitest;

/**
 * Created by user2004 on 20.04.16.
 */

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;


public interface API {

    @Multipart
    @POST("/")
    Call<Next> createUser(@Part("password") String password);
}